package sio.slam2;

/**
 * Classe de représentant des voitures
 * * d'une agence de location
 */
public class Voiture {
    private String immatriculation;
    private String modele;
    private int kilometrage;
    private Categorie laCategorie;

    /**
     * Constructeur
     * @param uneImmatriculation immatriculation
     * @param unModele modele
     * @param unKilometrage kilométrage
     * @param uneCategorie catégorie
    */
    public Voiture(String uneImmatriculation, String unModele, int unKilometrage,
                   Categorie uneCategorie){
        this.immatriculation = uneImmatriculation;
        this.modele = unModele;
        this.kilometrage = unKilometrage;
        this.laCategorie = uneCategorie;
    }

    public String getImmatriculation() {
        return immatriculation;
    }
    public String getModele() {
        return modele;
    }
    public int getKilometrage() {
        return kilometrage;
    }
    public Categorie getLaCategorie() {
        return laCategorie;
    }

    public void setKilometrage(int kilometrage) {
        this.kilometrage = kilometrage;
    }

    /**
     *
     * @return Représentation de la voiture sous forme de chaîne de caractères
     */
    public String toString() {
        return "La "+ this.modele +" immatriculée " +
                this.immatriculation + " a un kilométrage de "+
                this.kilometrage
                +"kms. C'est une voiture de catégorie "+ this.laCategorie.getLibelle();
    }

    /*La twingo immatriculée AA-000-BB a un kilométrage de 2700kms. C'est une voiture
    de catégorie Citadine.*/

    /**
     * Calcul du tarif horaire de la location en fonction
     * de la catégorie de la voiture et de son kilométrage.
     * Les voitures ayant un kilométrage de 30000 kms ou plus bénéficient d'une réduction de 20 %
     * Les voitures ayant un kilométrage de 50000 kms ou plus bénéficient d'une réduction de 50 %     *
     * @return tarif horaire de location de la voiture
     */
    private float tarifHoraire() {

        float tarif= this.laCategorie.getTarifHoraire();

        if (this.kilometrage >= 50000){
            tarif = (float) (tarif*(1-0.5));
        }else
        {
            if (this.kilometrage>=30000){
                tarif = (float) (tarif* (1-0.2));
            }
        }

        return tarif;
    }

    /**
     * Calcule du coût d'une location
     * @param nbHeures durée de la location
     * @return coût total de la location
     */
    public float tarifLocation(int nbHeures){

        return nbHeures * this.tarifHoraire();

    }

}
