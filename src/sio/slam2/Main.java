package sio.slam2;

/** * Test des classes Voiture et Categorie */
public class Main {
    public static void main(String[] args) {
        Categorie c1 = new Categorie("Citadine", 25);
        Voiture v1 = new Voiture("AA-000-BB", "twingo", 2700, c1);
        Voiture v2 = new Voiture("CC-111-DD", "208", 45000, c1);
        Voiture v3 = new Voiture("EE-222-FF", "polo", 57000, c1);
        Voiture v4 = new Voiture("BB-123-DD", "swift", 30000, c1);
        Voiture v5 = new Voiture("BB-123-DD", "swift", 50000, c1);

        //v4.setKilometrage(50000);
        assert v1.tarifLocation(10) == 250 : "Erreur v1";
        assert v2.tarifLocation(10) == 250 * 0.8 : "Erreur v2";
        assert v3.tarifLocation(10) == 125 : "Erreur v3";
        assert v4.tarifLocation(10) == 250 * 0.8 : "Erreur v4";
        assert v5.tarifLocation(10) == 125 : "Erreur v5";

        // A COMPLETER

    }
}