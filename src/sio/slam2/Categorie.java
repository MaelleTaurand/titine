package sio.slam2;

/**
 * Classe représentant des catégories de voiture
 */

public class Categorie {
    private String libelle;
    private float tarifHoraire;
    /**
     * Constructeur
     * @param unLibelle nom de la catégorie
     * @param unTarifHoraire tarif horaire de location
     */
    public Categorie(String unLibelle, float unTarifHoraire) {
        this.libelle = unLibelle;
        this.tarifHoraire = unTarifHoraire;

    }
    public String getLibelle() {
        return libelle;
    }

    public float getTarifHoraire() {
        return this.tarifHoraire;
    }

    /*
    *
    * @return Représentation de la Catégorie sous forme de chaîne de caractères
    */
    public String toString(){
        return "Le tarif horaire de la categorie "
                + this.libelle
                + " est de "
                + this.tarifHoraire
                + " euros";
    }


}
